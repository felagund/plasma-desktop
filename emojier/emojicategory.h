/*
    SPDX-FileCopyrightText: 2022 Weng Xuetian <wegnxt@gmail.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef EMOJICATEGORY_H
#define EMOJICATEGORY_H
#include <KLocalizedString>
#include <QStringList>

const QStringList &getCategoryNames();

#endif
